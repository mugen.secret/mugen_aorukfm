3067

0(-912):リダイレクトで指定されてる場所。nameの指定。authornameの指定。利用animの指定。
4:プレイヤーＩＤ
8:自分のいる並び順の番号。デバッグ表示＋１
12:teamside
16:		（本体は０でヘルパーだとマイナス１）
20:
24:
28:ishelper（本体だと落ちる）←destroyselfやparentvarset実行に必要。!ishelperでnumpartnerに換算される。
32-76(-904):デバッグで表示上の名前
([P+0]+604)でキャッシュ位置
80:const(data.life)
84:const(data.power)
88:const(data.attack)
92:const(size.attack.z.width.front)
96:const(size.attack.z.width.back)
100:const(data.airjuggle)
104:const(size.attack.dist)
108:const(size.proj.attack.dist)
112:const(data.defence)
116:const(data.fall.defence_mul)
120:const(data.liedown.time)
124:const(size.xscale)
128:const(size.yscale)
132:const(size.ground.back)
136:const(size.ground.front)
140:const(size.air.back)
144:const(size.air.front)
148:const(size.z.width)
152:const(size.height)
156:const(size.proj.doscale)
160:const(size.head.pos.x)
164:const(size.head.pos.y)
168:const(size.mid.pos.x)
172:const(size.mid.pos.y)
176:const(size.shadowoffset)
180:const(velocity.walk.fwd.x)
184:const(velocity.walk.back.x)
188:const(velocity.walk.up.x)
192:const(velocity.walk.down.x)
196:const(velocity.run.fwd.x)
200:const(velocity.run.fwd.y)
204:const(velocity.run.back.x)
208:const(velocity.run.back.y)
212:const(velocity.run.down.x)
216:const(velocity.run.down.y)
220:const(velocity.run.up.x)
224:const(velocity.run.up.y)
228:const(velocity.jump.y)
232:const(velocity.jump.neu.x)
236:const(velocity.jump.fwd.x)
240:const(velocity.jump.back.x)
244:const(velocity.jump.up.x)
248:const(velocity.jump.down.x)
252:const(velocity.runjump.y)
256:const(velocity.runjump.fwd.x)
260:const(velocity.runjump.back.x)
264:const(velocity.runjump.up.x)
268:const(velocity.runjump.down.x)
272:const(movement.airjump.num)
276:const(movement.airjump.height)
280:const(velocity.airjump.y)
284:const(velocity.airjump.neu.x)
288:const(velocity.airjump.fwd.x)
292:const(velocity.airjump.back.x)
296:const(velocity.airjump.up.x)
300:const(velocity.airjump.down.x)
304:const(movement.yaccel)
308:const(movement.stand.friction)
312:const(movement.crouch.friction)
316:const(data.sparkno)
320:const(data.guard.sparkno)
324:const(data.KO.echo)
328:const(size.draw.offset.x)
332:const(size.draw.offset.y)
336:const(data.IntPersistindex)
340:const(data.FloatPersistIndex)
344:プレイヤー存在フラグ
348:pauseで止まっているかどうか
352(-824):ライフ
356:最大ライフ
360:表示上のライフ
364:ライフの赤い部分(下手にいじると落ちるので注意)
368:パワー参照先(ヘルパーの場合はここの値に関わらず本体のここを利用する)
372:パワーマックス参照先
376:パワー
380:最大パワー
384:表示上のパワー
388:パワーレベル(Power/1000をLv.1として値、毎F上書きされている)

400:facing
412:screenpos x
416:pos y
424:screenpos xに対応？弄っても変化なし
428:pos yに対応？弄っても変化なし
432:pos z
436:vel x
440:vel y
444:vel z
460:sprpriority
476:pausemovetime
480:supermovetime
540:projectile管理領域
572:keyctrl
708-748:command
3048:利用するステート指定
3052:奪われているステート
3056:誰にstを奪われているか判断するための数字（マイナス１以外のランダム値だと落ちる）
3060:stateno
3064:prevstateno
3580:time
3584:type
3588:movetype
3592:physics
3596:ctrl
3600:ここの部分の値が0以外だとガードステート化。ただしこのステートに居続けないと解除。
3604:
3608:hitpausetime
3612:hitpausetime(次フレームに付与？)
3616:posfreeze
3620:alive
3628:行動済みフラグ
3648:var(0)
3888:fvar(0)
4048:sysvar(0)
4068:sysfvar(0)
4088:特殊無敵（superpauseのunhittable・投げ属性）
4092(111):hitby
4096:hitby2
4100:hitby-time
4104:hitby2-time
4120:gethitvar(type)
4124:gethitvar(animtype)
4128:gethitvar(airtype)
4132:gethitvar(groundtype)
4136:gethitvar(damage)
4140:gethitvar(hitcount)
4144:gethitvar(fallcount)
4148:gethitvar(hitshaketime)
4152:gethitvar(hittime)
4156:gethitvar(slidetime)
4160:gethitvar(ctrltime)

4184:GetHitVar(XVel)

4196:gethitvar(chainid)
4200:gethitvar(guarded)
4208(140):hitfall;gethitvar(fall)
4212:gethitvar(fall.damage)
4216:gethitvar(fall.xvel)
4224:gethitvar(fall.recover)
4228:gethitvar(fall.time) 
4232:gethitvar(fall.recovertime)
4236:gethitvar(fall.kill) 

4244:gethitvar(fall.envshake.time)
4248:gethitvar(fall.envshake.dir)
4252:gethitvar(fall.envshake.freq)

4264:hitoverride	slot=0	何か。実行中の合図？
4268:hitoverride	slot=0	attr
4272:hitoverride	slot=0	state
4276:hitoverride	slot=0	time
4280+20*n:hitoverride	slot=n	何か。実行中の合図？
4440:hitdef,reversaldef実行フラグ
4448:AffectTeam(F:1, E:2, B:3)
4452:attr
4524:自己pausetime(hitdef,reversaldef共通)
4584:p2stateno
4612:reversal.attr
4812:guard.dist
4988:angledraw回転値
5020(343):hitcount
5024(344):uniqhitcount
5052:animアドレス
5056:changeanim2アドレス
5060:palnoの値

5064(354):拡張領域1

5504(464):拡張領域2

5944:フリー

9752:helperID
9756:親のＩＤ
9760:parentリダイレクト
9764(1529):rootリダイレクト
9768:playertype
9772:aiフラグ
9776:aiレベル
13408:standby
13348:p2リダイレクト
13352:enemyリダイレクト
13356:roundsexisted

4938572	親捏造１回目の取得場所（Ｘとする）
Ｘ	C:\MUGEN(ＭＵＧＥＮの置いてある場所)
Ｘ+43284	親捏造１回目記録値
Ｘ+43284-3844（+39440）	win.time	勝利メッセージ
Ｘ+43284-3840（+39444）	ctrl.time	試合開始からコントロールまで
Ｘ+43284-3836（+39448）	slow.time	ＫＯ後スロー
Ｘ+43284-3832（+39452）	over.waittime	試合後コントロール
Ｘ+43284-3828（+39456）	over.hittime	試合後回復可能
Ｘ+43284-3824（+39460）	over.wintime	判定
Ｘ+43284-3820（+39464）	over.time	試合後演出
Ｘ+43284+2792（+46076）	gametime
Ｘ+43284+3648（+46932）	１Ｐアドレス記録（親捏造２回目中継）
Ｘ+43284+4156（+47440）	何番目のプレイヤーから存在しているか
Ｘ+43284+4160（+47444）	何番目のプレイヤーから存在しているか
Ｘ+43284+4428（+47712）	次に召喚されるヘルパーid
Ｘ+43284+4688（+47972）	ヘルパー召喚フラグ
Ｘ+43284+4708（+47992）	assertspecial	intro
Ｘ+43284+4709（+47993）	assertspecial	roundnotover
Ｘ+43284+4710（+47994）	assertspecial	noko
Ｘ+43284+4715（+47999）	assertspecial	timerfreeze
Ｘ+43284+4716（+48000）	assertspecial	nobardisplay
Ｘ+43284+4800（+48084）	pausetime
Ｘ+43284+4836（+48120）	superpausetime
Ｘ+43284+4844（+48128）	pausetime
Ｘ+43284+4848（+48132）	roundno
Ｘ+43284+4852（+48136）	勝利数１Ｐ
Ｘ+43284+4856（+48140）	勝利数２Ｐ
Ｘ+43284+4892（+48176）	roundstate
Ｘ+43284+4896（+48180）	勝利チーム
Ｘ+43284+4900（+48184）	試合の終わり方（１：ＫＯ　２：ＤＫＯ　３：タイムオーバー）
Ｘ+43284+4908（+48192）	残り時間、フレーム
Ｘ+43284+4924（+48208）	１Ｐ勝ち方（０：Ｖ　１：Ｓ　２：Ｈ　３：Ｃ　４：Ｔ　５：投げ　６：髑髏　７：自殺）
Ｘ+43284+4964（+48248）	２Ｐ勝ち方
Ｘ+43284+5004（+48288）	１Ｐ勝ち方補足（８：パーフェクト）
Ｘ+43284+5044（+48328）	２Ｐ勝ち方補足
Ｘ+43284+5180（+48464）	画面の大きさ（４：通常　５：小さい）
Ｘ+43284+8224（+51508）	ゲームスピード設定
